package tareaautonomasistemasdistribuidos;

public class TareaAutonomaSistemasDistribuidos {

    public static void main(String[] args) {
        
        //No determinista
        NoDeterminista nd = new NoDeterminista();
        System.out.println("-----No Determinista------");
        
        Thread nd1 = new Thread(){
            public void run(){
            nd.add(2);
            System.out.println("A: "+ nd.getCount());
            }
        };
    
        Thread nd2 = new Thread(){
            public void run(){
            nd.add(3);
            System.out.println("B: "+ nd.getCount());
            }
        };
        
        nd1.start();
        nd2.start();
       
        
        //Determinista
        Determinista determinista = new Determinista();
        System.out.println("-------Determinista------");
        
        Thread d1 = new Thread(){
            public void run(){
            determinista.add(2);
            System.out.println("A: "+ determinista.getCount());
            }
        };
    
        Thread d2 = new Thread(){
            public void run(){
            determinista.add(3);
            System.out.println("B: "+ determinista.getCount());
            }
        };
        
       
        d1.start();
        d2.start();
        
    }
}
